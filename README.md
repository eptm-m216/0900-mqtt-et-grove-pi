# 0900 MQTT et Grove Pi

![grovepi](img/image1.jpg)

## Objectifs

- Interagir avec ses capteurs et ses sorties
- connecter au serveur MQTT

## Ressources

* [Documentation Git](https://git-scm.com/book/en/v2)
* [Documentation Grove Pi](https://dexterind.github.io/GrovePi/api/gpio/)
* [Documentation paho-mqtt](https://pypi.org/project/paho-mqtt)

## Matériel
- Poste de travail
- Dongle WiFi
- Kit GrovePi et Raspberry Zero W
- Logiciel MQTTX
- Logiciel Win32DiskImager
- Logiciel Putty
- Logiciel FileZilla

## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre <visa> est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom
* [- Si ce n'est pas fait, récuperez les fichier -] `D:\iso\grovepi[...].img`, `D:\tools\win32-disk-imager[...].exe` et `D:\tools\FileZilla[...].exe` [-  sur votre disque SSD personnel -]

## Introduction

L'objectif de cet exercice est du cumuler toutes vos connaissances acquises durant le cours afin de faire intergir les capteurs du Grove Pi avec le serveur MQTT de l'école.

Nous allons simuler une pièce d'une maison IoT, par groupe de deux. Voici son fonctionnement :
* Le potentiomètre va simuler un capteur la température de la pièce, avec des données comprises entre -20°C et 40°C.
* Un bouton simulera un interrupteur d'éclairage.
* Une LED va simuler un éclairage, elle s'allumera lors de l'appui par votre binôme. 
* L'écran LCD va afficher la température de votre binôme


## Pratique

### Git
- Effectuez un fork de ce dépôt

### GrovePi
- Connectez le potentiomètre sur le port A0
- Connectez le bouton sur le port A1
- Connectez une LED sur le port D3
- Connecter l’écran LCD sur le port I2C

![grovepi](img/image3.jpg)

## Script

Modifiez le script mqtt_led_pot:
* remplacez `<visa_send>` par votre `<visa>`
* remplacez `<visa_receive` par le `<visa>` de votre binôme
* Complétez les autres `# TODO` afin que votre script envoie les valeurs du bouton et du potentiomètre
* Complétez les autres `# TODO` afin que votre script reçoive les valeurs du bouton et du potentiomètre de votre binôme et allume la LED en conséquent