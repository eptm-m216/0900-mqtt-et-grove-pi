#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : tc_M216_p_0500_MQTT_send_f_v1a.py
#
# Auteur (Nom, Prénom)               : Jérémy Michaud
# Classe                             : MQTT_receive
# Module                             : M216
# Date de création                   : 30.01.2023
#
# Description succincte du programme :
#    Reçois des paquets MQTT selon les paramètres globaux
#----------------------------------------------------------------------------

import random
import time

from grovepi import *
from grove_rgb_lcd import *

from paho.mqtt import client as mqtt_client


broker = 'mqtt-eptm.jcloud.ik-server.com'
port = 11521

# TODO remplacez <visa_send> par votre visa
visa_send = '<visa_send>'

# TODO remplacez <visa_receive> par le visa de votre binôme
visa_receive = '<visa_receive>'

topic_pot_send = visa_send + "/pot"
topic_button_send = visa_send + "/button"
topic_pot_receive = visa_receive + "/pot"
topic_button_receive = visa_receive + "/button"
max_qos = 2

max_analog_read = 1023
max_temp = 40 # °C
min_temp = -20 # °C

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = ''
password = ''

connected = False

temperature = None

pinPot = 0
pinButton = 1
pinLed = 3

pinMode(pinPot, "INPUT")
pinMode(pinButton, "INPUT")
pinMode(pinLed, "OUTPUT")

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        if msg.topic == topic_pot_receive :
            # TODO
            pass
        elif msg.topic == topic_button_receive :
            # TODO
            pass

    client.subscribe(topic_pot_receive, max_qos)
    client.subscribe(topic_button_receive, max_qos)
    client.on_message = on_message
    
def readPin(pin, min, max, rnd = 0):
    return round(analogRead(pin) / max_analog_read * (max - min) + min, rnd)
    
def formatTemp(val):
    return str(val).rjust(3)

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_start()
    old_but = -1
    old_pot = -1
    old_temp = None
    refresh = True
    while True:
        pot = int(readPin(pinPot, min_temp, max_temp))
        but = analogRead(pinButton)
        if(old_but != but):
            refresh = True
            old_but = but
            # TODO

        if(old_pot != pot):
            refresh = True
            old_pot = pot
            # TODO
            
        if(old_temp != temperature):
            refresh = True
            old_temp = temperature

        if(refresh):
            refresh = False
            butStr = str(int(but != 0))
            potStr = formatTemp(pot)
            tempStr = formatTemp(temperature) + " C"
            setText_norefresh("Btn:" + butStr +"    Pot:"+ potStr + "\nTemp:" + tempStr)
            print("\rButton: " + butStr + "   |    Pot: "+ potStr + "   |    Temp: " + tempStr + "     ", end="", flush=True)

        time.sleep(0.3)

if __name__ == '__main__':
    run()
